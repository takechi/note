%Root Constitutive_equation_and_Isotropy.tex
\section{テンソル不変量}
	任意のテンソルは直交変換に対して不変ではないが、テンソルからスカラー量、すなわち
	直交変換に対して不変な量を生成することはできる。これをテンソル不変量と呼ぶ。
	
	一般に$n$階テンソルについて不変量が存在すると思われるが、ここでは$2$階テンソルの不変量を
	考える。
	\subsection{$2$階テンソルの不変量}
	$2$階のテンソル$\boldsymbol{t}$について、その固有値は直交変換について不変である。
	\begin{proof}
		直交変換$\boldsymbol{\Omega}$は、
		\begin{align*}
			t_{ij} \rightarrow t^{\prime}_{ij}
				&= \Omega_{ik} \Omega_{jl} t_{kl} \\
				&= \Omega_{ik} t_{kl} (\Omega^{-1})_{lj}
		\end{align*}
		であるので、見やすいように太字で書けば
		\begin{align*}
			\boldsymbol{t} \rightarrow \boldsymbol{t}^{\prime} = \boldsymbol{\Omega} \boldsymbol{t} \boldsymbol{\Omega}^{-1}
		\end{align*}
		となる。これは相似変換と呼ばれる。
		$\boldsymbol{t}$の固有値は固有方程式
		\begin{align*}
			\det (\boldsymbol{t} - \lambda \boldsymbol{E}) = 0
		\end{align*}
		の解なので、この左辺(固有多項式)が不変であれば、固有値も不変である\footnote{
			ここで$\boldsymbol{E}$は単位行列を表す。
		}。
		
		相似変換に対して固有多項式は
		\begin{align*}
			\det (\boldsymbol{\Omega} \boldsymbol{t} \boldsymbol{\Omega}^{-1} - \lambda \boldsymbol{E})
				&= \det ( \boldsymbol{\Omega} (\boldsymbol{t} - \lambda \boldsymbol{E}) \boldsymbol{\Omega}^{-1}) \\
				&= \det (\boldsymbol{\Omega}) \det (\boldsymbol{t} - \lambda \boldsymbol{E}) \det (\boldsymbol{\Omega}^{-1}) \\
				&= \det  (\boldsymbol{t} - \lambda \boldsymbol{E})
		\end{align*}
		となり、不変。よって、固有値は直交変換に対し不変となる。
	\end{proof}
	特性多項式を真面目に展開すると
	\begin{align*}
		\det(\boldsymbol{t} - \lambda \boldsymbol{E})
			&= \left|
				\begin{array}{ccc}
					t_{11} - \lambda & t_{12} & t_{13} \\
					t_{21} & t_{22} - \lambda & t_{23} \\
					t_{31} & t_{32} & t_{33} - \lambda
				\end{array}
				\right| \\
			&= (t_{11} - \lambda) (t_{22} - \lambda) (t_{33} - \lambda) + t_{12} t_{23} t_{31} + t_{13} t_{32} t_{21} \\
				&\hspace{1em} - t_{13} (t_{22} - \lambda) t_{31} - t_{12} t_{21} (t_{33} - \lambda) - t_{23} t_{32} (t_{11} - \lambda) \\
			&= -\lambda^{3} + (t_{11} + t_{22} + t_{33}) \lambda^{2} \\
				&\hspace{1em} - (t_{11} t_{22} + t_{22} t_{33} + t_{33} t_{11} + t_{13} t_{31} + t_{12} t_{21} + t_{23} t_{32}) \lambda \\
				&\hspace{1em} + (t_{11} t_{22} t_{33} + t_{12} t_{23} t_{31} + t_{12} t_{32} t_{21} - t_{13} t_{31} t_{22} - t_{12} t_{21} t_{33} - t_{23} t_{32} t_{11}) \\
			&= -\lambda^{3} + \tr \boldsymbol{t} \  \lambda^{2} - \dfrac{1}{2} \clr{\lr{\tr \boldsymbol{t}}^{2} - \tr \boldsymbol{t}^{2}} \lambda + \det(\boldsymbol{t}) \\
			&\equiv -\lambda^{3} + t_{\I} \lambda^{2} - t_{\II} \lambda + t_{\III}.
	\end{align*}
	ここで
	\begin{align*}
		t_{\I} &\equiv \tr \boldsymbol{t} = t_{ii} \\
		t_{\II} &\equiv \dfrac{1}{2} \clr{\lr{\tr \boldsymbol{t}}^{2} - \tr \boldsymbol{t}^{2}} = \dfrac{1}{2} (t_{ii} t_{jj} - t_{ij} t_{ji}) \\
		t_{\III} &\equiv \det \boldsymbol{t} = \dfrac{1}{6} \epsilon_{ijk} \epsilon_{lmn} t_{il} t_{jm} t_{kn}
	\end{align*}
	と定義した。解と係数の関係から、$t_{\I}, t_{\II}, t_{\III}$も座標変換に対して不変であり、これらをそれぞれ
	第$1$, 第$2$, 第$3$テンソル不変量と呼ぶ。この$3$つは独立な不変量であり、一般に、$\boldsymbol{t}$から
	生成できるテンソル不変量は全て$t_{\I}, t_{\II}, t_{\III}$の関数として書ける。
	
	$t_{\I}, t_{\II}, t_{\III}$の代わりに
	\begin{align*}
		t_{(1)} &\equiv t_{ii} = \tr \boldsymbol{t} \\
		t_{(2)} &\equiv t_{ij} t_{ji} = \tr \boldsymbol{t}^{2} \\
		t_{(3)} &\equiv t_{ij} t_{jk} t_{ki} = \tr \boldsymbol{t}^{3}
	\end{align*}
	を独立なテンソル不変量ととることもできる。なぜなら
	\begin{align*}
		t_{\I} &= t_{(1)} \\
		t_{\II} &= \dfrac{1}{2} \clr{t_{(2)} - \lr{t_{(1)}}^{2}} \\
		t_{\III} &= \dfrac{1}{6} \clr{2 t_{(3)} - 3 t_{(2)} t_{(1)} + \lr{t_{(1)}}^{3}}
	\end{align*}
	となり、$t_{\I}, t_{\II}, t_{\III}$による表示は常に$t_{(1)}, t_{(2)}, t_{(3)}$による表示に
	変換できる。
	形式的な計算では、$t_{(1)}, t_{(2)}, t_{(3)}$を用いると楽になる場合が多い\footnote{
		実際に応力テンソルなどを考えるときに重要になってくるのは$t_{\I}, t_{\II}, t_{\III}$の方である。
	}。

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "Constitutive_equation_and_Isotropy"
%%% End: 