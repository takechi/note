%Root Constitutive_equation_and_Isotropy.tex
\section{構成方程式}
	構成方程式とは、歪みを表すテンソル$\varepsilon_{ij}$と応力テンソル$\sigma_{ij}$の関係を
	与える方程式である\footnote{
		"歪みを表す"と曖昧に書いたのは、弾性論と流体力学ではその定義が違うためである。
		弾性論では変位場の微分で与えられるので歪みテンソル、流体力学では速度場の微分で
		与えられるので歪み速度テンソルとなる。
		なお、階数も文脈も違うので明らかだが、$\varepsilon_{ij}$と擬テンソルの項で定義した
		$\varepsilon_{ijk}$とは全く関係ない。
	}。つまり、
	\begin{align*}
		\boldsymbol{\sigma} = \boldsymbol{\sigma}(\boldsymbol{\varepsilon})
	\end{align*}
	である。後で示すように$\varepsilon_{ij}$は対称テンソルなので、これは対称$2$階テンソルから$2$階テンソルを返す写像であり、
        前節までの議論をそのまま使うことができる。
	
	よって、考えている連続体が等方物質であれば、構成方程式の形は
	\begin{align}
		\sigma_{ij} = f \delta_{ij} + g \varepsilon_{ij} + h \varepsilon_{ik} \varepsilon_{kj} \label{eqn.constitutive_rep}
	\end{align}
	となる。この事実は$1945$年にM. Reinerが初めて示したらしい\footnote{
		M. Reiner, "A Mathematical Theory of Dilatancy", Amer. J. Math. {\bf 67}(1945), 350--362
	}。
	
	この\eqn{constitutive_rep}へ色々な近似やモデル化を組み込んだものが、
	物理としての実在弾性体、実在流体の構成方程式となり、それを運動量保存の式に
	代入すればNavier-Stokes方程式などと呼ばれるものになる。
	
	\subsection{Newton流体}
		流体力学において完全流体の次に基本的な流体がNewton流体である。
		Newton流体での仮定は、「応力は歪み速度の$1$次まで」という仮定である。
		これより、
		\begin{align*}
			f &= f(\varepsilon_{(1)}, \varepsilon_{(2)}, \varepsilon_{(3)}) \equiv -p + \lambda \varepsilon_{(1)} \\
			g &= \mathrm{const} \equiv 2 \mu \\
			h &= 0
		\end{align*}
		となる\footnote{
			$f$は$\boldsymbol{\varepsilon}$の$0$次の係数なので、$\boldsymbol{\varepsilon}$の$1$次関数となり、
			$g$は$\boldsymbol{\varepsilon}$の$1$次の係数なので、スカラーとなる。
			$h$は$\boldsymbol{\varepsilon}$の$2$次の係数なので、$0$でなければならない。
		}。
		よって、
		\begin{align}
			\sigma_{ij} = -p \delta_{ij} + \lambda \varepsilon_{kk} \delta_{ij} + 2 \mu \varepsilon_{ij}. \label{eqn.sigam-varepsilon}
		\end{align}
		を得る。これがNewtonの仮定と呼ばれるもので、$p$は静水圧、$\lambda$は第$2$粘性率、$\mu$は粘性率と
		呼ばれる。この構成方程式と運動量保存の式\footnote{
			$\rho$は質量密度、$v_{i}$は速度場、$F_{i}$は外部からの体積力、$\mathrm{D}/\mathrm{D}t$はLagrange微分。
		}
		\begin{align*}
			\rho \dfrac{\mathrm{D} v_{i}}{\mathrm{D} t} = \partial_{j} \sigma_{ji} + \rho F_{i}
		\end{align*}
		および歪み速度$\varepsilon_{ij}$の定義
		\begin{align*}
			\varepsilon_{ij} = \dfrac{1}{2} \lr{\partial_{i} v_{j} + \partial_{j} v_{i}}
		\end{align*}
		から
		\begin{align*}
			\rho \dfrac{\mathrm{D} v_{i}}{\mathrm{D} t}
				&= -\partial_{i} p + 2 \mu \partial_{j} \varepsilon_{ij} + \lambda \partial_{i} \varepsilon_{jj} + \rho F_{i} \\
				&= -\partial_{i} p + \mu \partial_{j} \partial_{j} v_{i} + (\mu + \lambda)\partial_{i} \partial_{j} v_{j} + \rho F_{i}
		\end{align*}
		となる。これがNavier-Stokes方程式である。非圧縮性流体であれば
		\begin{align*}
			\partial_{i} v_{i} = 0
		\end{align*}
		であるので
		\begin{align*}
			\rho \dfrac{\mathrm{D} v_{i}}{\mathrm{D} t} = -\partial_{i} p + \mu \partial_{j} \partial_{j} v_{i} + \rho F_{i}
		\end{align*}
		である。
	\subsection{Hook弾性体}
		弾性体力学での最も簡単なモデルはHook弾性体である。
		Hook弾性体での仮定は、すなわちHookの法則であり、「応力は歪みの$1$次」という仮定である。
		
		Newton流体とどこが違うのかというと、速度場ではなく変位場を使う点、およびそれに伴って
		静水圧$p = 0$となる点である\footnote{
			弾性体を考えているので、歪みが$0$なら応力も$0$というのは自然な仮定である。よって、
			\eqn{sigam-varepsilon}から
			\begin{align*}
				\sigma_{ij}(\varepsilon_{ij} = 0) = -p \delta_{ij} = 0
			\end{align*}
			なので、$p = 0$となる。
		}。構成方程式は
		\begin{align*}
			\sigma_{ij} = \lambda \varepsilon_{kk} \delta_{ij} + 2 \mu \varepsilon_{ij}.
		\end{align*}
		弾性論の文脈では、$\lambda, \mu$はLam\'e定数\footnote{特に$\mu$は剛性率などと呼ばれる。}と
		呼ばれる。

	\subsection{まとめ}
	一般に等方性連続体であるという仮定だけから、構成方程式の自由度は$3$変数関数$3$つにまで落とせる。
	更に、Newton流体やHook弾性体は構成方程式に線形という制限を加え、$3$変数関数$3$つの自由度を
	スカラー変数$3$つ、あるいは$2$つの自由度にまで落としている。
		
	非Newton流体や塑性体などの構成方程式も、全て\eqn{constitutive_rep}に対して
	実験事実を外挿したり、何らかのモデルを立てたりすることで得られる。

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "Constitutive_equation_and_Isotropy"
%%% End: 