%Root Constitutive_equation_and_Isotropy.tex

\section{等方テンソル}
\subsection{テンソルとは}
	ここでは、テンソルを直交変換に対する変換性によって定義する。
\subsection{直交変換}
	直交変換とは、任意のベクトルの内積を保存するような座標の変換のことである。
	直感的に言えば、座標の回転と鏡像反転である。
	
	直交と名前が付いているように、直交行列と関係があり、次のことが示せる。
	\paragraph{定理}
	ベクトルの内積を保存する座標変換は、常に直交行列で与えられる。
	\begin{proof}
		座標系$O$において$2$つのベクトル$a_{i}, b_{i}$を考える。
		行列$\Omega_{ij}$によって座標変換を行うとすると、新しい座標系$O^{\prime}$
		から見たベクトル$a_{i}, b_{i}$はそれぞれ
		\begin{align*}
			a_{i} &\rightarrow a^{\prime}_{i} = \Omega_{ij} a_{j} \\
			b_{i} &\rightarrow b^{\prime}_{i} = \Omega_{ij} b_{j}
		\end{align*}
		となる。このとき、$2$つのベクトルの内積が保存すると仮定すると、
		\begin{align*}
			a^{\prime}_{i} b^{\prime}_{i}
				&= \Omega_{ij} a_{j} \Omega_{ik} b_{k} \\
				&= \Omega_{ij} \Omega_{ik} a_{j} b_{k} \\
				&= a_{j} b_{j}
		\end{align*}
		となるので、
		\begin{align}
			\Omega_{ij} \Omega_{ik} = \delta_{jk} \label{eqn.orthogonal_relation1}
		\end{align}
		でなければならない。
                なお、$\delta_{ij}$はKloneckerの$\delta$で、
		\begin{align*}
			\delta_{ij} = \left\{
			\begin{array}{cl}
				1 & (i = j) \\
				0 & (i \neq j)
			\end{array}
			\right.
		\end{align*}
		と定義される。
                この両辺に$\Omega_{kl}$の逆行列$\Omega^{-1}_{kl}$を
		かけると
		\begin{align}
			\Omega_{ij} \Omega_{ik} \Omega^{-1}_{kl} &= \delta_{jk} \Omega^{-1}_{kl} \nonumber \\
			\Omega_{ij} \delta_{il} &= \delta_{jk} \Omega^{-1}_{kl} \nonumber \\
			\Omega_{lj} &= \Omega^{-1}_{jl}. \label{eqn.def_orthogonal_matrix}
		\end{align}
		すなわち、
		$\Omega_{ij}$は直交行列である。


		逆に、直交行列による変換は常に内積を保存することは、この証明を逆に
		たどることにより明らかである。
	\end{proof}
	$a^{\prime}_{i}$から$a_{i}$への変換が$\Omega_{ij}$の逆行列、つまり転置行列で
	与えられることを使えば
	\begin{align}
		\Omega_{ij} \Omega_{kj} = \delta_{ik} \label{eqn.orthogonal_relation2}
	\end{align}
	を得る。\eqn{orthogonal_relation1}、\eqn{orthogonal_relation2}は
	直交関係と呼ばれ、以下の計算においてしばしば利用する。
	
	\eqn{orthogonal_relation1}、\eqn{orthogonal_relation2}からすぐに解るように、
	直交行列の行列式は
	\begin{align*}
		( \det \boldsymbol{\Omega} )^2 &= 1 \\
                \det \boldsymbol{\Omega} &= \pm 1
	\end{align*}
	である。
	\subsubsection{回転変換}
		直交変換の中で、変換行列の行列式が$1$になるものを回転変換と呼ぶ。
		\subsubsection{Euler角での表現}
			剛体力学などでは、座標の回転はEuler角によって与えられることが多い。
			$i$軸周りに$\theta$だけ座標を回転する変換行列を$\boldsymbol{\Omega}^{(i)}(\theta)$と書くと、
			\begin{align*}
				\boldsymbol{\Omega}^{(1)}(\theta) &= \left(
				\begin{array}{ccc}
					1 & 0 & 0 \\
					0 & \cos \theta & \sin \theta \\
					0 & -\sin \theta & \cos \theta
				\end{array} \right), \hspace{1em}
				\boldsymbol{\Omega}^{(2)}(\theta) = \left(
				\begin{array}{ccc}
					\cos \theta & 0 & -\sin \theta \\
					0 & 1 & 0 \\
					\sin \theta & 0 & \cos \theta
				\end{array} \right),\\
				\boldsymbol{\Omega}^{(3)}(\theta) &= \left(
				\begin{array}{ccc}
					\cos \theta & \sin \theta & 0 \\
					-\sin \theta & \cos \theta & 0 \\
					0 & 0 & 1
				\end{array} \right)
			\end{align*}
			であり、Euler角による表現と言えば、普通
			\begin{align*}
				\boldsymbol{\Omega}(\phi, \theta, \psi)
				&= \boldsymbol{\Omega}^{(3)}(\phi) \boldsymbol{\Omega}^{(1)}(\theta) \boldsymbol{\Omega}^{(3)}(\psi) \\
				&= \left(
				\begin{array}{ccc}
					\cos \psi \cos \theta - \sin \psi \cos \theta \sin \phi & \cos \psi \sin \phi + \sin \psi \cos \theta \cos \phi & \sin \psi \sin \theta \\
					-\sin \psi \cos \theta - \cos \phi \cos \theta \sin \phi & -\sin \psi \cos \phi + \cos \psi \cos \theta \cos \phi & \cos \psi \sin \theta \\
					\sin \theta \sin \phi & -\sin \theta \cos \phi & \cos \theta
				\end{array} \right)
			\end{align*}
			と定義される。
	\subsubsection{鏡像変換}
		直交変換の中で、回転変換ではないもの、つまり行列式が$-1$であるものは、
		回転変換と鏡像変換の組み合わせで与えられる。
		
		鏡像変換の最も簡単なものとして、$xy$平面での鏡像変換を行列で書けば
		\begin{align}
			\boldsymbol{M}^{(3)} \equiv \left(
			\begin{array}{ccc}
				1 & 0 & 0 \\
				0 & 1 & 0 \\
				0 & 0 & -1
			\end{array}
			\right) \label{eqn.mirror_3}
		\end{align}
		となる。
		
		鏡像変換を$2$回行うと、その変換行列の行列式は勿論$1$であり、すなわち回転行列となる\footnote{
                  当たり前のようだが、何だか不思議な気もする。
		}。
\subsection{テンソルの定義}
	$a_{i}$が、任意の直交変換$\Omega_{ij}$によって
	\begin{align*}
		a^{\prime}_{i} = \Omega_{ij} a_{j}
	\end{align*}
	と変換されるとき、$a_{i}$は$1$階のテンソルであると言う\footnote{
		この定義は特に「テンソル場」を考えるときに有効になってくる。
		例えば$a_{i}(\vec{x}) = x_{i}^{3}$という場を考えると、
		\begin{align*}
			a^{\prime}_{i}(\vec{x}^{\prime})
				&= {x^{\prime}_{i}}^{3} \\
				&= \Omega_{ij} \Omega_{ik} \Omega_{il} x_{j} x_{k} x_{l} \hspace{1em}(\text{ただし、$i$について和はとらない}) \\
				&\neq \Omega_{ij} x_{j}^{3}
		\end{align*}
		となり、これはテンソル場ではない。
		添え字付きの量(配列)として表されるという情報だけでは、テンソルかどうか決めることはできない。
		（後述の擬テンソルの項も参照のこと。）
	}。
	また、$t_{ij}$が任意の直交変換$\Omega_{ij}$によって
	\begin{align*}
		t^{\prime}_{ij} = \Omega_{ik} \Omega_{jl} t_{kl}
	\end{align*}
	と変換されるとき、$t_{ij}$は$2$階のテンソルであると言う。
	
	同様にして、$t_{i_{1} i_{2} \dots i_{n}}$が$n$階のテンソルである条件は、
	任意の直交変換$\Omega_{ij}$によって
	\begin{align}
		t^{\prime}_{i_{1} i_{2} \dots i_{n}} = \Omega_{i_{1} a_{1}} \Omega_{i_{2} a_{2}} \cdots \Omega_{i_{n} a_{n}} t_{a_{1} a_{2} \ldots a_{n}} \label{eqn.def_tensor}
	\end{align}
	と変換されることである\footnote{この考え方から、スカラーは$0$階テンソルとして定義できる。}。
\subsection{等方テンソル}
	ある量が等方的であるとは、回転変換の前後によってその量が
	不変となることである\footnote{
		文献\cite{Smith.1968}などでは、直交変換について不変なテンソルをisotropic tensorと定義し、
		回転変換について不変なテンソルはrotation tensorと呼んでいる。
	}。
	
	すなわち、$n$階等方テンソルの条件は
	\begin{align*}
		t^{\prime}_{i_{1} i_{2} \dots i_{n}}
			&= \Omega_{i_{1} a_{1}} \Omega_{i_{2} a_{2}} \cdots \Omega_{i_{n} a_{n}} t_{a_{1} a_{2} \ldots a_{n}} \\
			&= t_{i_{1} i_{2} \dots i_{n}}
	\end{align*}
	となることである。

	\subsubsection{$0$階等方テンソル}
		$0$階テンソルすなわちスカラーは、その定義から回転変換について不変であり、
		常に等方的である。
	\subsubsection{$1$階等方テンソル}
		$1$階テンソルすなわちベクトルは、$0$ベクトルを除き、常に非等方的である。
		\begin{proof}
			$a_{i}$が等方的であると仮定する。
			\begin{align*}
				\boldsymbol{\Omega}^{(3)}(\pi) = \left(
				\begin{array}{ccc}
					-1 & 0 & 0 \\
					0 & -1 & 0 \\
					0 & 0 & 1
				\end{array} \right)
			\end{align*}
			であるので、この変換に対して$a_{i}$が不変であるとすると
			\begin{align*}
				\left(
				\begin{array}{c}
					a^{\prime}_{1} \\ a^{\prime}_{2} \\ a^{\prime}_{3}
				\end{array} \right) =
				\left(
				\begin{array}{ccc}
					-1 & 0 & 0 \\
					0 & -1 & 0 \\
					0 & 0 & 1
				\end{array} \right) \left(
				\begin{array}{c}
					a_{1} \\ a_{2} \\ a_{3}
				\end{array} \right) = \left(
				\begin{array}{c}
					-a_{1} \\ -a_{2} \\ a_{3}
				\end{array} \right) = \left(
				\begin{array}{c}
					a_{1} \\ a_{2} \\ a_{3}
				\end{array} \right).
			\end{align*}
			よって、$a_{1} = a_{2} = 0.$同様にして$\boldsymbol{\Omega}^{(1)}(\pi)$などを使えば$a_{3} = 0$が示せて、
			等方的な$1$階テンソルは$0$ベクトルしかないことが解る。
			なお、全ての成分が$0$のテンソルは一般に自明な等方テンソルである。
		\end{proof}
	\subsubsection{$2$階等方テンソル}
		$2$階等方テンソルは次の表式
		\begin{align*}
			t_{ij} = A \delta_{ij}
		\end{align*}
		で与えられる。$A$は任意のスカラーである。
		\begin{proof}
			$t_{ij}$が等方的であると仮定する。$t_{ij}$の各成分を
			\begin{align*}
				i = j &\ \text{つまり} \ t_{11}, t_{22}, t_{33} \\
				i \neq j &\ \text{つまり} \ t_{12}, t_{13}, t_{21}, t_{23}, t_{31}, t_{32}
			\end{align*}
			の$2$つに分ける。
			
			$i = j$のグループについて、等方性から$t_{11} = t_{22} = t_{33}.$ これを$A$と置く。
			
			$i \neq j$のグループの中で、$t_{31}$を考える。
			\begin{align*}
				\boldsymbol{\Omega}^{(3)}(\pi) = \left(
				\begin{array}{ccc}
					-1 & 0 & 0 \\
					0 & -1 & 0 \\
					0 & 0 & 1
				\end{array} \right)
			\end{align*}
			で変換すると
			\begin{align*}
				t^{\prime}_{31}
					&= \Omega^{(3)}_{3k}(\pi) \Omega^{(3)}_{1l} t_{kl} \\
					&= -t_{31}.
			\end{align*}
			よって、$t_{31} = 0.$同様にして、他の$i \neq j$成分は$0$となり、結局
			\begin{align*}
				t_{ij} = A \delta_{ij}.
			\end{align*}
			
			逆に$t_{ij} = A \delta_{ji}$が等方的であることは、
			回転変換$\Omega_{ij}$の直交関係\eqn{orthogonal_relation2}を使って
			\begin{align*}
				t^{\prime}_{ij}
					&= \Omega_{ik} \Omega_{jl} t_{kl} \\
					&= A \Omega_{ik} \Omega_{jk} \\
					&= A \delta_{ij} = t_{ij}
			\end{align*}
			となり、示せる。
		\end{proof}
	\subsubsection{$3$階等方テンソル}
		$3$階等方テンソルは次の表式
		\begin{align*}
			T_{ijk} = A \epsilon_{ijk}
		\end{align*}
		に限られる。ここで$A$は任意のスカラー、$\epsilon_{ijk}$はEddingtonの$\epsilon$で
		\begin{align}
			\epsilon_{ijk} = \left\{
			\begin{array}{cl}
				1 & ((i, j, k) = (1, 2, 3), (2, 3, 1), (3, 1, 2)) \\
				-1 & ((i, j, k) = (1, 3, 2), (3, 2, 1), (2, 1, 3)) \\
				0 & (\mathrm{otherwise})
			\end{array}
			\right. \label{eqn.def_epsilon}
		\end{align}
		と定義される\footnote{Levi-Civitaの記号、交代記号(Permutation Tensor)、
		完全反対称テンソルなどとも呼ばれる。}。
		\begin{proof}
			$T_{ijk}$の各成分を次の$3$つに分ける。
			\begin{align*}
				\begin{array}{ll}
					(\mathrm{i}) \text{全ての成分が同じ} & T_{111}, T_{222}, T_{333} \\
					(\mathrm{ii}) \text{$3$つの中$2$つが同じ} & T_{112}, T_{113}, \ldots \text{(全部で$18$個)} \\
					(\mathrm{iii}) \text{全ての成分が異なる} & T_{123}, T_{231}, \ldots \text{(全部で$6$個)}
				\end{array}
			\end{align*}

			$(\mathrm{i})$について、
			\begin{align*}
				T^{\prime}_{111}
					&= \Omega^{(3)}_{1l}(\pi) \Omega^{(3)}_{1m}(\pi) \Omega^{(3)}_{1n}(\pi) T_{lmn} \\
					&= (-1) (-1) (-1) T_{111} \\
					&= -T_{111}
			\end{align*}
			であるので、$T_{111} = 0.$同様にして、他の$2$成分も$0$となる。
			
			$(\mathrm{ii})$について
			\begin{align*}
				T^{\prime}_{112}
					&= \Omega^{(3)}_{1l}(\pi) \Omega^{(3)}_{1m}(\pi) \Omega^{(3)}_{2n}(\pi) T_{lmn} \\
					&= (-1) (-1) (-1) T_{112} \\
					&= -T_{112}.
			\end{align*}
			よって$T_{112} = 0.$同様にして、他の$17$成分も$0.$
			
			$(\mathrm{iii})$について、
			\begin{align*}
				\boldsymbol{\Omega}\lr{\dfrac{\pi}{2}, \dfrac{\pi}{2}, 0} = \left(
				\begin{array}{ccc}
					0 & 1 & 0 \\
					0 & 0 & 1 \\
					1 & 0 & 0
				\end{array}
				\right)
			\end{align*}
			となり、これを使えば
			\begin{align*}
				T_{123} = T_{231} = T_{312} \equiv B \\
				T_{132} = T_{321} = T_{213} \equiv C
			\end{align*}
			を得る。
			ここで
			\begin{align*}
				\boldsymbol{\Omega}^{(3)}\lr{\dfrac{\pi}{4}} = \left(
				\begin{array}{ccc}
					\dfrac{\sqrt{2}}{2} & \dfrac{\sqrt{2}}{2} & 0 \vspace{0.5em} \\
					-\dfrac{\sqrt{2}}{2} & \dfrac{\sqrt{2}}{2} & 0 \vspace{0.5em} \\
					0 & 0 & 1
				\end{array}
				\right) \equiv \tilde{\boldsymbol{\Omega}}
			\end{align*}
			を考えて
			\begin{align*}
				T^{\prime}_{123}
					&= \tilde{\Omega}_{1l} \tilde{\Omega}_{2m} \tilde{\Omega}_{3n} T_{lmn} \\
					& \hspace{2em} n = 3 \text{のみで非$0$より} \\
					&= \tilde{\Omega}_{11} \tilde{\Omega}_{21} T_{113}
						+ \tilde{\Omega}_{11} \tilde{\Omega}_{22} T_{123}
						+ \tilde{\Omega}_{12} \tilde{\Omega}_{21} T_{213}
						+ \tilde{\Omega}_{12} \tilde{\Omega}_{22} T_{223} \\
					& \hspace{2em} T_{113} = T_{223} = 0 \text{なので} \\
					&= \dfrac{1}{2} T_{123} - \dfrac{1}{2} T_{213} \\
					&= \dfrac{1}{2}\lr{B - C} = T_{123} = B
			\end{align*}
			となり、$B$と$C$についての関係式を得る。
			同様にして$T^{\prime}_{213}$から$B$と$C$についての関係式が導かれ、
			\begin{align*}
				\dfrac{1}{2} \lr{B - C} &= B \\
				-\dfrac{1}{2} \lr{B - C} &= C
			\end{align*}
			という連立方程式を得、これを解けば$B = -C$となる。ここで改めて$B = -C = A$とおくと、
			結局$T_{ijk} = A \epsilon_{ijk}$となる。
			
			逆に$T_{ijk} = A \epsilon_{ijk}$が等方的であることは
			\begin{align*}
				T^{\prime}_{ijk}
					&= \Omega_{il} \Omega_{jm} \Omega_{kn} A \epsilon_{lmn} \\
					&= A (\Omega_{i1} \Omega_{j2} \Omega_{k3} + \Omega_{i2} \Omega_{j3} \Omega_{k1} + \Omega_{i3} \Omega_{j1} \Omega_{k2} \\
					& \hspace{1em} - \Omega_{i1} \Omega_{j3} \Omega_{k2} - \Omega_{i3} \Omega_{j2} \Omega_{k1} - \Omega_{i2} \Omega_{j1} \Omega_{k3})
			\end{align*}
			であるので、
			\begin{align*}
				T^{\prime}_{123} &= A \det \boldsymbol{\Omega} = A \\
				T^{\prime}_{132} &= -A \det \boldsymbol{\Omega} = -A \\
				T^{\prime}_{111}
					&= A (\Omega_{11} \Omega_{12} \Omega_{13} + \Omega_{12} \Omega_{13} \Omega_{11} + \Omega_{13} \Omega_{11} \Omega_{12} \\
					& \hspace{1em} - \Omega_{11} \Omega_{13} \Omega_{12} - \Omega_{13} \Omega_{12} \Omega_{11} - \Omega_{12} \Omega_{11} \Omega_{13}) = 0\\
				T^{\prime}_{112}
					&= A (\Omega_{11} \Omega_{12} \Omega_{23} + \Omega_{12} \Omega_{13} \Omega_{21} + \Omega_{13} \Omega_{11} \Omega_{22} \\
					& \hspace{1em} - \Omega_{11} \Omega_{13} \Omega_{22} - \Omega_{13} \Omega_{12} \Omega_{21} - \Omega_{12} \Omega_{11} \Omega_{23}) = 0
			\end{align*}
			などとなって\footnote{逆に、行列式が$\epsilon_{ijk}$で定義されていると考えることもできる。
			すなわち、
			\begin{align*}
				\det \boldsymbol{X}
					&= \epsilon_{ijk} X_{i1} X_{j2} X_{k3} \\
					&= \dfrac{1}{6} \epsilon_{ijk} \epsilon_{lmn} X_{il} X_{jm} X_{kn}.
			\end{align*}
			}、$T^{\prime}_{ijk} = A \epsilon_{ijk} = T_{ijk}$であることが解る。
		\end{proof}
		
		なお、$\epsilon_{ijk}$は直交変換に対しては不変ではない。
		これは、鏡像変換\eqn{mirror_3}より
		\begin{align*}
			M^{(3)}_{il} M^{(3)}_{jm} M^{(3)}_{kn} \epsilon_{lmn} = -\epsilon_{ijk}
		\end{align*}
		となることからすぐに解る。
		
		\paragraph{擬テンソル}
			ある右手系の正規直交基底
			$(\vec{e}^{\,(1)}, \vec{e}^{\,(2)}, \vec{e}^{\,(3)})$で決まる座標系$O$において
			\begin{align*}
				\varepsilon_{ijk}
					&= (\vec{e}^{\,(i)} \times \vec{e}^{\,(j)}) \cdot \vec{e}^{\,(k)}
			\end{align*}
			という量を定義すると、これは$3$次元配列として$\epsilon_{ijk}$と等しいことが
			すぐに解る。しかし、$\varepsilon_{ijk}$と$\epsilon_{ijk}$は全く違うものである。
			と言うのは、$\epsilon_{ijk}$はその定義としてテンソルである。一方、$\varepsilon_{ijk}$は
			テンソルではない。これは$\varepsilon_{ijk}$の鏡像変換に対する変換性を見れば解る。
			
			鏡像変換\eqn{mirror_3}によって基底を
			\begin{align*}
                        (\vec{e}^{\,(1)}, \vec{e}^{\,(2)}, \vec{e}^{\,(3)}) 
                        \rightarrow (\vec{e}^{\,(1) \prime}, \vec{e}^{\,(2) \prime}, \vec{e}^{\,(3) \prime}) = (\vec{e}^{\,(1)}, \vec{e}^{\,(2)}, -\vec{e}^{\,(3)})
			\end{align*}
			と左手系に変換し、この新しい基底で決まる座標系$O^{\prime}$から$\varepsilon_{ijk}$を決めると、
			外積および内積の定義と、基底の正規直交性から
			\begin{align*}
				\varepsilon_{ijk} \rightarrow \varepsilon^{\prime}_{ijk}
					&= (\vec{e}^{\,(i) \prime} \times \vec{e}^{\,(j) \prime}) \cdot \vec{e}^{\,(k) \prime} \\
					&= \varepsilon_{ijk}
			\end{align*}
			であり、鏡像変換について$\varepsilon_{ijk}$は変化しない。
			
			一方、$\varepsilon_{ijk}$がテンソルであるとすると、テンソルの定義\eqn{def_tensor}から
			\begin{align*}
				\varepsilon^{\prime \prime}_{ijk}
					&= M^{(3)}_{il} M^{(3)}_{jm} M^{(3)}_{kn} \varepsilon_{lmn} \\
					&= -\varepsilon_{ijk}
			\end{align*}
			でなければならない。よって、$\varepsilon_{ijk}$はテンソルではない
			\footnote{$\epsilon_{ijk}$はテンソルであるが、そのために鏡像変換に対して
			不変でない。逆に、$\varepsilon_{ijk}$は鏡像変換を含む任意の直交変換に対して不変だが、
			そのためにテンソルではない。}。
			
			$\varepsilon_{ijk}$のように、直交変換$\Omega_{ij}$によって
			\begin{align*}
				t^{\prime}_{i_{1} i_{2} \dots i_{n}}
					&= \det (\boldsymbol{\Omega}) \Omega_{i_{1} a_{1}} \Omega_{i_{2} a_{2}} \cdots \Omega_{i_{n} a_{n}} t_{a_{1} a_{2} \ldots a_{n}} \\
					&= \pm \Omega_{i_{1} a_{1}} \Omega_{i_{2} a_{2}} \cdots \Omega_{i_{n} a_{n}} t_{a_{1} a_{2} \ldots a_{n}}
			\end{align*}
			と変換されるものを、擬テンソルあるいは軸性テンソルと呼ぶ\footnote{
                          軸性テンソルに対して通常のテンソルのことを特に極性テンソルと呼ぶことがある。
                          $\varepsilon_{ijk}$が擬テンソルになる原因は外積$\times$の性質にあり、
                          一般に（極性ベクトル）$\times$（極性ベクトル）$=$（軸性ベクトル）である。
                        }。
	\subsubsection{高階等方テンソル}
		$4$階等方テンソルは
		\begin{align}
			t_{ijkl} = A \delta_{ij} \delta_{kl} + B \delta_{ik} \delta_{jl} + C \delta_{il} \delta_{jk} \label{eqn.4th_isotropic_tensor}
		\end{align}
		の形に限られる。
		
		$5$階等方テンソルは
		\begin{align}
			t_{ijklm} =
				C_{1} \epsilon_{ijk} \delta_{lm}
				+ C_{2} \epsilon_{ijl} \delta_{km}
				+ C_{3} \epsilon_{ijm} \delta_{kl}
				+ C_{4} \epsilon_{ikl} \delta_{jm}
				+ C_{5} \epsilon_{ikm} \delta_{jl}
				+ C_{6} \epsilon_{ilm} \delta_{jk} \label{eqn.5th_isotropic_tensor}
		\end{align}
		の形に限られる。
		
		$6$階等方テンソルは
		\begin{align}
			t_{ijklmn} &=
				C_{1} \delta_{ij} \delta_{kl} \delta_{mn}
				+ C_{2} \delta_{ij} \delta_{km} \delta_{ln}
				+ C_{3} \delta_{ij} \delta_{kn} \delta_{lm}
				+ C_{4} \delta_{ik} \delta_{jl} \delta_{mn} \nonumber \\
				&\hspace{1em} + C_{5} \delta_{ik} \delta_{jm} \delta_{ln} 
				+ C_{6} \delta_{ik} \delta_{jn} \delta_{lm}
				+ C_{7} \delta_{il} \delta_{jk} \delta_{mn}
				+ C_{8} \delta_{il} \delta_{jm} \delta_{kn} \nonumber \\
				&\hspace{1em} + C_{9} \delta_{il} \delta_{jn} \delta_{km}
				+ C_{10} \delta_{im} \delta_{jk} \delta_{ln}
				+ C_{11} \delta_{im} \delta_{jl} \delta_{kn}
				+ C_{12} \delta_{im} \delta_{jn} \delta_{kl} \nonumber \\
				&\hspace{1em} + C_{13} \delta_{in} \delta_{jk} \delta_{lm}
				+ C_{14} \delta_{in} \delta_{jl} \delta_{km}
				+ C_{15} \delta_{in} \delta_{jm} \delta_{kl} \label{eqn.6th_isotropic_tensor}
		\end{align}
		の形に限られる。
		ただし、(\ref{eqn.4th_isotropic_tensor}), (\ref{eqn.5th_isotropic_tensor}), (\ref{eqn.6th_isotropic_tensor})式で
		$A, B, C, C_{i}(i = 1, 2, \ldots, 15)$はスカラーである。
		
		証明は、{\bf 原理的には}$3$階までと同じようにやればできるが、果てしないので
		省略。
		
		\paragraph{数学的背景}
			一般に、$k \in \mathbb{N}$のとき、$2k$階の等方テンソルは$k$個のKroneckerの$\delta$で
			表すことができ、$2k + 1$階の等方テンソルは$k - 1$個のKroneckerの$\delta$と
			$1$個のEddingtonの$\epsilon$で表すことができる\mycite{Andrews.1981}。
			
			すなわち、$n$階の等方テンソルは
			\begin{align*}
				n : \mathrm{even} &\hspace{1em}  T_{i_{1} i_{2} \cdots i_{n}} = \clr{\delta_{i_{1} i_{2}} \delta_{i_{3} i_{4}} \cdots \delta_{i_{n - 1} i_{n}}, \text{(添え字の組み合わせ)}}\text{の線形結合} \\
				n : \mathrm{odd} &\hspace{1em}  T_{i_{1} i_{2} \cdots i_{n}} = \clr{\delta_{i_{1} i_{2}} \delta_{i_{3} i_{4}} \cdots \epsilon_{i_{n - 2} i_{n - 1} i_{n}}, \text{(添え字の組み合わせ)}}\text{の線形結合} \\
			\end{align*}
			という形で与えられる。
			
			ただし、一般に全ての添え字の組み合わせによってできるテンソルが\underline{独立であるとは限らない}\,\mycite{Smith.1968}。
			
			例えば、$n = 5$において$\clr{i, j, k, l, m}$を$2$個と$3$個(つまり、$\delta$と$\epsilon$)に分ける組み合わせは
			$5! / (3! 2!) = 10個$、つまり
			\begin{align*}
                                &
				\epsilon_{ijk} \delta_{lm}, \hspace{1em}
				\epsilon_{ijl} \delta_{km}, \hspace{1em}
				\epsilon_{ijm} \delta_{kl}, \hspace{1em}
				\epsilon_{ikl} \delta_{jm}, \hspace{1em}
				\epsilon_{ikm} \delta_{jl}, \\
                                &
				\epsilon_{ilm} \delta_{jk}, \hspace{1em}
				\epsilon_{jkl} \delta_{im}, \hspace{1em}
				\epsilon_{jkm} \delta_{il}, \hspace{1em}
				\epsilon_{jlm} \delta_{ik}, \hspace{1em}
				\epsilon_{klm} \delta_{ij}
			\end{align*}
			である。
			しかし、これらの間には
			\begin{align*}
				\epsilon_{jkl} \delta_{im} &= \epsilon_{ijk} \delta_{lm} - \epsilon_{ijl} \delta_{km} + \epsilon_{ikl} \delta_{jm} \\
				\epsilon_{jkm} \delta_{il} &= \epsilon_{ijk} \delta_{lm} - \epsilon_{ijm} \delta_{kl} + \epsilon_{ikm} \delta_{jl} \\
				\epsilon_{jlm} \delta_{ik} &= \epsilon_{ijl} \delta_{km} - \epsilon_{ijm} \delta_{km} + \epsilon_{ilm} \delta_{jk} \\
				\epsilon_{klm} \delta_{ij} &= \epsilon_{ikl} \delta_{jm} - \epsilon_{ikm} \delta_{jl} + \epsilon_{ilm} \delta_{jk}
			\end{align*}
			という関係があり\mycite{Healy.1975}、このため独立な成分は$6$個となる。
			
			一般に、$n$階等方テンソルの独立な成分の個数を$Q_{n}$と書く\footnote{
				$Q_{n}$はMotzkin sumあるいはRiordan number, ring numberなどと呼ばれるものと等しいらしい。
				Motzkin sumというのは、Motzkin数$M_{n}$を、円上の$n$個の点の間に交差をさせることなく
				弦を描く方法の場合の数(ただし、全ての点を使って弦を描く必要はない)、と定義したとき$M_{n} = Q_{n} + Q_{n + 1}$となるために
				こう呼ばれるらしい。
			}と、
			\begin{align*}
				Q_{n} = \dfrac{n - 1}{n + 1} \lr{2 Q_{n - 1} + 3 Q_{n - 2}} \hspace{1.5em} (Q_{0} = 1, \ Q_{1} = 0)
			\end{align*}
			となる\mycite{Weisstein_isotropic_tensor}。これは級数を使って
			\begin{align*}
				Q_{n} = \sum_{r = 0}^{\lfloor n / 2 \rfloor} \dfrac{n! (3 r - n + 1)}{(n - 2 r)! r! (r + 1)!}
			\end{align*}
                        または
                        \begin{align*}
                        Q_{n} = \dfrac{1}{n + 1} \sum_{k = 1}^{\lfloor n / 2 \rfloor}
					\binom{n + 1}{k}
                                        \binom{n - k - 1}{k - 1}
                         \end{align*}
			とも書ける\mycite{Andrews.1981}\footnote{
				$\lfloor x \rfloor$は床関数で、$x$以下の最大の整数を返す。
                                また、$\binom{n}{k}$は$2$項係数で、
                                \begin{align*}
                                \binom{n}{k}
                                = {}_n \mathrm{C}_k = \dfrac{n!}{(n-k)!k!}
                                \end{align*}
                                である。
			}。
			
			\begin{table}[h]
				\begin{center}
					\begin{tabular}{c|ccccccccccc}
						$n$ & $0$ & $1$ & $2$ & $3$ & $4$ & $5$ & $6$ & $7$ & $8$ & $9$ & $10$ \\ \hline
						$Q_{n}$ & $1$ & $0$ & $1$ & $1$ & $3$ & $6$ & $15$ & $36$ & $91$ & $232$ & $603$ 
					\end{tabular}
					\caption{$n$階等方テンソルの独立成分の個数$Q_{n}$ \label{tab.Q_n}}
				\end{center}
			\end{table}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "Constitutive_equation_and_Isotropy"
%%% End: 
