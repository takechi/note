f(x)= 1./((exp(x)+1.) * (exp(-x)+1.))
set samples 1000
set xlab "XLAB"
set ylab "YLAB"
unset key
set xran [-30:30]
plot f(x)
set term postscript eps enhanced color
set output "dnde.eps"
replot