z32     = 2.612375348685488343349
z322    = z32 * z32
z52     = 1.341487257250917179757
z52_z32 = z52 / z32
p2      = 3.141592653589793238462 * 2.
p4      = p2 * 2.

Cvu(x)=3./2.*((3.*z52_z32 - z322/p4) + (3./2.*z52_z32 - z322/p4)*x**(-1.5) + (z322/p2 - 2.*z52_z32)*x**(-3.))
Cvd(x)=15./4. * x ** 1.5 * z52_z32
Cv(x) = (x > 1 ? Cvu(x) : Cvd(x))

set ytics 0.5
set xlab "XLAB"
set ylab "YLAB"
unset key

set xran [0:10]
plot Cv(x)

set term postscript eps enhanced color
set output "C_V.eps"
replot