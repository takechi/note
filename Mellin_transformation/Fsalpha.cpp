#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

int main(){
  std::string outputfn = "Fsalpha.txt";
  double s = 1.5;
  int nMax = 1000000;
  double alphaMax = 8.0;
  int sample = 1000;

  std::ofstream ofs(outputfn.c_str());

  double delta = alphaMax / sample;

  for(int i = 0; i <= sample; ++i){
    double alpha = i * delta;

    double Fs = 0.0;
    for(int n = 1; n <= nMax; ++n){
      Fs += ::exp(-n * alpha) / ::pow(n, s);
    }
    
    ofs << alpha << " " << Fs << std::endl;
  }

  return 0;
}
