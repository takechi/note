set parametric

set zeroaxis
unset border
unset tics
unset key

set xran[-1.2:1.2]
set yran[-1.2:1.2]
set size square


set style arrow 1 size character 4,30
set style arrow 2 size character 3,20

set arrow 1 from 0.6, -1 to 0.6, 0.3 arrowstyle 1
set arrow 2 from 0.6, 0.3 to 0.6, 1.0 nohead 

set arrow 3 from -1.2, 0 to 1.2, 0 arrowstyle 2
set arrow 4 from 0, -1.2 to 0, 1.2 arrowstyle 2

set label "O"  at -0.10, -0.10 left
set label "Re" at  1.05,  0.10 left
set label "Im" at -0.15,  1.10 left

set arrow 5 from 0.6, 0 to cos(2.4) + 0.6 , sin(2.4) nohead

set label "RR" at 0.3, 0.4 left

set label "sc" at 0.5, -0.1 left

set label "lc" at 0.65, -0.6 left

plot [pi/2.:3.*pi/2.] cos(t) + 0.6, sin(t) lc rgb "black"

set term postscript eps enhanced color
set output "int_path.eps"
replot