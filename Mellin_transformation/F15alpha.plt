set xran [0:8]
set sample 1000
set xlab "XLAB"
set ylab "YLAB"
unset key
plot "Fsalpha.txt" using ($1):($2) w l
set term postscript eps enhanced color
set output "F15alpha.eps"
replot